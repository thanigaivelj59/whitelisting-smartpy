import { Tezos, TezosOperationError } from "@taquito/taquito";

import conf from "../conf/conf";

import euroTzFA12Storage from "../smartContracts/euroTzContract/euroTzFA12Storage";
import euroTzFA12Code from "../smartContracts/euroTzContract/euroTzFA12Code";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

Tezos.importKey(conf.adminSecretKey);

export async function originateContract() {
  try {
    console.log("Begin euroTz origination operation...");

    const originationOp = await Tezos.contract.originate({
      code: euroTzFA12Code,
      init: euroTzFA12Storage,
    });

    const contractOriginationOp = await originationOp.contract();

    console.log("New euroTz contractAddress :", contractOriginationOp.address);

    console.log("End euroTz origination operation.");
    return contractOriginationOp.address;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log("MESSAGE: ", e.errors);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

originateContract();
