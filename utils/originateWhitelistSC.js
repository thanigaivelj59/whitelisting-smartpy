import { Tezos, TezosOperationError } from "@taquito/taquito";

import conf from "../conf/conf";

import whitelistCode from "../smartContracts/whitelistContract/whitelistCode";
import whitelistStorage from "../smartContracts/whitelistContract/whitelistStorage";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

Tezos.importKey(conf.adminSecretKey);

export async function originateContract() {
  try {
    console.log("Begin whitelist contract origination operation...");

    const originationOp = await Tezos.contract.originate({
      code: whitelistCode,
      init: whitelistStorage,
    });

    const contractOriginationOp = await originationOp.contract();

    console.log(
      "New whitelistContractAddress :",
      contractOriginationOp.address
    );

    console.log("End whitelist contract origination operation.");
    return contractOriginationOp.address;
  } catch (e) {
    if (e instanceof TezosOperationError) {
      console.log("MESSAGE: ", e.errors);
    } else {
      console.log("PublicNode Error ", e);
    }
  }
}

originateContract();
