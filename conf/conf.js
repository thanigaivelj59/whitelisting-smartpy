export default {
  // Contracts

  whitelistContractAddress: "...Pass whitelist contract here...",
  euroTzContractAddress: "...Pass euroTz contract here...",

  lorentzWhitelistContractAddres: "KT1LGYKH5TLGDnnAZonVF5L4y9aYT5ZYmwuR",
  lorentzTokenContractAddres: "KT1DDaf5JgrMFtAdq4Y5NyXhw2yxghcVejcB",

  viewAdminAddress: "KT1Qddu25qk6wtFwyoLyp8PYPrwCLwQMTNWX",
  viewIssuerAddress: "KT1UUXprFtT9uaUVsCoyMkFvYrSUZPn9QTNv",
  viewUserWhitelistAddress: "KT1NgVL97e79DhMEYdkDr3wwHmR5cvaiUAxU",
  viewWhitelistDetailsAddress: "KT1CiayN5TQbFx8qggQb5jvjG9an4JNf9y5U",

  // Users

  // Admin credentials
  adminAddress: "tz1SVqTz7entj982jDSKcTQNgT7f2cg7C8dk",
  adminPublicKey: "edpkudkbM1JPwxchDKZt5J7FjVP2K8G2q8fEforMZX8osvbkJeGRD1",
  adminSecretKeyUnencrypted:
    "edsk34kAz18pFQBtDLiU41v6Sm4rGvZ3bnKsG5koj4TWjC59TQ4svj",
  adminSecretKey:
    "edskRioMPrRYbibwR7EqrokJtq3f8a66LeUvm1Sxya3pTgvWz7pTdghfNAvahAHNf3iox3j7P9nkYrKeCiaHVtiMu7yZraNYRe",

  // Fred credentials
  fredAddress: "tz1XrCvviH8CqoHMSKpKuznLArEa1yR9U7ep",
  fredSecretKeyUnencrypted:
    "edsk3bpNdqNfX5aPiFZJvszyAKSYrj1cuWzX5VLUaTfpYx39h8aVmq",
  fredPublicKey: "edpkvNYwBeVWcrn3SS55mUyi8qBiTZdQbzDgkRpW65xLG2W2c7aA6u",
  fredSecretKey:
    "edskRt2ibNbv4BNrn7FRXuYTwGF54oVAe8JuUuE8YVsvnc78GwpRvYk9ftmQKxQjeczGZwacGSdHWWXrLDWcYoq3EhiY5TuUtU",

  // Ouss credentials
  oussAddress: "tz1XvMBRHwmXtXS2K6XYZdmcc5kdwB9STFJu",
  oussSecretKey: "edsk2wW5nyKq7VEwXJntkiGkUb52nP92ukYDWNTRNWXZzJrsZheqHD",
  oussPrivateKey:
    "edskRgeZRQ7YtDGzz9qesBeRBhQUXmeNDzmXDGyfzFNBHbvnKEx6ofQUrg3aVeuMS2afNh3zNjZpVT2HBYTXfZD7zr5ye6ZB5Z",
  oussPublicKey: "edpkud9ThGW8zBxucFnssPusUQZmijuSpAdL2xekQS6wRuTi1TdvYq",

  // Khaled credentials:
  khaledAddress: "tz1RmD9igqvhQ4FkWw7GMQxxoenvHj6N478g",
  khaledSecretKey: "edsk3SuUN2GstegvesSimN5NzkxoCtUyFr3yfdBkqpXg4FyNpckfXd",
  khaledPrivateKey:
    "edskRqPAbJTG4fpHqgWdJHLVSTGaTcmYDwj1XYC9KWcqZbuc6VvtBGvvRQ76ApEmLYD53ixHopM1kw4PufJoreMcpui5QBjJnq",
  khaledPublicKey: "edpku16FzRvRSdqMHFLmR1SimEaLUrvishG8Yuu9sFgLdCEPiJdPvP",
  thib: {
    sk:
      "edskRvx9HPpHfFTtWGBwinfuDyVqwxbzjb31oE1r7ahHcKQnVUhtKLcAfaJcRccG953H2G38ySd8CoBzso2iiMaGpUPCXR749S",
    pk: "edpkvJodASV7nm1HNxdQXK7ogDghrJnxBvSWuBUWznC6d1WWkreTeS",
    pkh: "tz1MKu4bY94ShvWUG9aEVW5QFa6ae66evbaP",
    skh: "edsk3meiDuBcqJt8qb6isvKDtK3XKkC4hFm7UAwQmSbUovtJPkgXtG",
  },
  daly: {
    sk:
      "edskRmxovqi7zSaugvYRm2FGNGxob5yUF2iDVuQwSSRLmfWPEkZHxfAqKnx2WKniW936t9fgza16mZeX5iXddSHpraNhX6HNP1",
    pk: "edpkuQQgwAQ9x5tRn5uBKRfytVNwQSwkQYfCdcJe9ZUK7JxCzAKiv1",
    pkh: "tz1SvWtVSRA2gw1HqhPWMwK5oGryDUVYbm5H",
    skh: "edsk3FPkDRH7FinHJ9L9hkgCw2xe1Z8zDx7wKrPeJy1JTXJ1QDvzbR",
  },
  safwen: {
    sk:
      "edskRvmVhhpA8sXFJAggZYsWPPiQRPCg6N81xo7H9oPMDrJP1z3fjxiBjmNRvP1Fs9iUKc64SkASv1o2MbGzrHgZDWdSWPZCC1",
    pk: "edpktf4qCrvpDVYYWsm7xQFNnQKJ3eBo3W8VmfnJqtrpSki2ajMxbA",
    pkh: "tz1Wb3atMy4rqepc7Xq5Atas6W4fr3fRaDhP",
    skh: "edsk3m2sG5PK8sXcKsNhHy8bY1vgSao3bekqTXvGd8N8nk4W9kUxqs",
  },
  bob: {
    sk:
      "edskS2m7S5hW9cD3i1cDcbXxdKDibzswMNbiZh1Gcn4VjwtqNmUYFRC1w3Qc3gGu4ys4cVPGfp4U2PDZbKmUzdnnkGcWMYnojx",
    pk: "edpkuNDZVC1je9jpf54vgRTprmosKmgD4LjGF7n3Hm4KFPSnjW1uQM",
    pkh: "tz1fuR5WS2gzdqAKeXC8h7dtqpCCUxnXDyWo",
    skh: "edsk47D3hL1Z9Ga7QnVmvZZaGxgmjcRxjiSQVjWWHeoBzHJjPJc8rt",
  },
  alice: {
    sk:
      "edskRq65kLaSaFCeFPi23YJhNXBsiwhR2pBZEMfUbiviWxfnAe7aut184dSHuDq2LsP5ZoiGC2A7hvom9iX684frSUjqi2mac5",
    pk: "edpkuChBo1kE6KADpJ85BuFpHNz6Gv1cdMsDs6gnSZjJvG416pZCN6",
    pkh: "tz1fkHDxE65rAYVbs9EpjVRjUA5YhTdiNB6P",
    skh: "edsk3RuxtFHEWxusw1myJzpSrr7o4VfiZxn7hkE322timFAM54gJ5c",
  },
  khalil: {
    sk:
      "edskS2PzwbgUK8pKUxwzQhiGeiChbaMiHA9zchFRwjj4JfWFdEFtQqrxi9ZYAmGoLeEWwsYfCqaNUrKaKGGjjrYEyTHGSUNtzk",
    pk: "edpktsSkLRCvgZbxYz4RGy84Pa1iY5DAifbZKpGTuaHXYf4uxYNc8T",
    pkh: "tz1UgRSxJBsUxiC7AAF8Gi4zmuKtSjrGWtyn",
    skh: "edsk45yyh55P1LXSqsjdCHqPqQh4jSmTvzkr9zAowDz1E2fCpifcgZ",
  },
  jean: {
    sk:
      "edskRzGSJJoktQyyc3vLL1iYwMGEirPeAHpw1ng7AbmSRARxhyBeoCFoGSYKdWdH2EfPCD5VBhYpP4AWQQWQ9KwQwVsN7SGjzW",
    pk: "edpkvPM125S4ToyMLR5vWfaA5XicNpJvuFusLtEpxw5Eoqf1DRdo9d",
    pkh: "tz1R9LWdTcG932xiYBnZsACoKFNhnmkx2D87",
    skh: "edsk3xp2cyGW3rJxiU7Yo6oiWtfTbERUEJVEa79AhR8AZjCnXCHMQN",
  },

  // RPC config

  localNodeRPC: "http://localhost:8732",
  remoteNodeRPC: "https://carthagenet.SmartPy.io",
};
