import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";
import { BigNumber } from "bignumber.js";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("Whitelist Smart Contract: AddUser Entrypoint tests", function () {
  let whitelistContract;
  let whitelistStorage;

  before(async function () {
    whitelistContract = await Tezos.contract.at(conf.whitelistContractAddress);
  });

  it("Add user as non-admin / Should fail", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await whitelistContract.methods.addUser(conf.fredAddress).send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Add Issuer as standard user as admin / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods.addUser(conf.adminAddress).send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Add User with None as admin / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .addUser(conf.khalil.pkh)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);

      expect(whitelistContractOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Add user with Some as admin / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .addUser(conf.jean.pkh, "10")
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);

      whitelistStorage = await whitelistContract.storage();
      const userWhitelistID = await whitelistStorage.users.get(conf.jean.pkh);
      console.log(
        "userWhitelistID: ",
        new BigNumber(userWhitelistID).toNumber()
      );

      expect(whitelistContractOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
