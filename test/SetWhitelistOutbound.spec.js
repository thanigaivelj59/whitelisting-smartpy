import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("Whitelist Smart Contract: SetWhitelistOutbound Entrypoint tests", function () {
  let whitelistContract;
  let whitelistStorage;

  before(async function () {
    whitelistContract = await Tezos.contract.at(conf.whitelistContractAddress);
  });

  it("Set Whitelist Outbound as non-admin / Should fail", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await whitelistContract.methods.setWhitelistOutbound("666").send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Set Whitelist Outbound as admin with Some / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setWhitelistOutbound("666", [], true)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);

      whitelistStorage = await whitelistContract.storage();
      const whitelistDetails = await whitelistStorage.whitelists.get("666");
      console.log("whitelistDetails: ", whitelistDetails);

      expect(whitelistContractOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Update existing outbound whitelists as admin / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setWhitelistOutbound("666", ["555"], true)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);

      whitelistStorage = await whitelistContract.storage();
      const whitelistDetails = await whitelistStorage.whitelists.get("666");
      console.log("whitelistDetails: ", whitelistDetails);

      expect(whitelistContractOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Remove outbound whitelist as admin / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setWhitelistOutbound("666")
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);

      expect(whitelistContractOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
