import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("Whitelist Smart Contract: SetIssuer Entrypoint tests", function () {
  let whitelistContract;
  let whitelistStorage;

  before(async function () {
    whitelistContract = await Tezos.contract.at(conf.whitelistContractAddress);
    whitelistStorage = await whitelistContract.storage();
    const initialIssuer = whitelistStorage.issuer;
    console.log("initialIssuer: ", initialIssuer);
    console.log("------------------------------------------------------------");
  });

  it("Update issuer as non-admin / Should fail", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await whitelistContract.methods.setIssuer(conf.fredAddress).send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Update issuer as admin / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setIssuer(conf.fredAddress)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);

      whitelistStorage = await whitelistContract.storage();
      const newIssuer = whitelistStorage.issuer;
      console.log("Issuer from storage: ", newIssuer);

      expect(whitelistContractOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Reset old issuer as admin / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setIssuer(conf.adminAddress)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);

      whitelistStorage = await whitelistContract.storage();
      const newIssuer = whitelistStorage.issuer;
      console.log("Issuer from storage: ", newIssuer);

      expect(whitelistContractOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
