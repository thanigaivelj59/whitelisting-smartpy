import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("Whitelist Smart Contract: AssertReceiver Entrypoint tests", function () {
  let euroTzContract;
  let whitelistContract;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.euroTzContractAddress);
    whitelistContract = await Tezos.contract.at(conf.whitelistContractAddress);
  });

  it("Assert receiver Issuer - Admin mints 50 euroTz to the Issuer / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const euroTzMint = await euroTzContract.methods
        .mint(conf.adminAddress, "50")
        .send();

      await euroTzMint.confirmation();

      const euroTzMintHash = euroTzMint.hash;

      console.log("euroTzMintHash : ", euroTzMintHash);

      expect(euroTzMintHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert receiver unexistant User - Admin tries to mint 40 euroTz to an unexistant user / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await euroTzContract.methods.mint(conf.oussAddress, "40").send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin adds Ouss in users big_map", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const addUserOp = await whitelistContract.methods
        .addUser(conf.oussAddress, "1")
        .send();

      await addUserOp.confirmation();

      const addUserOpHash = addUserOp.hash;

      console.log("addUserOpHash : ", addUserOpHash);

      expect(addUserOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert receiver existant User, his associated whitelistID don't refer to an existing whitelist - Admin tries to mint 40 euroTz / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await euroTzContract.methods.mint(conf.oussAddress, "40").send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Ouss's whitelist outbound initally restricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("1", [], false)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert receiver existant User, his associated whitelist is restricted - Admin tries to mint 40 euroTz to whitelisted restricted user (Ouss) / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await euroTzContract.methods.mint(conf.oussAddress, "40").send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Ouss's whitelist outbound unrestricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("1", [], true)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert receiver existant User, the associated whitelistID refers to an existing whitelist and unrestricted - Admin mints 50 euroTz to Ouss: whitelisted unrestricted user / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const euroTzMint = await euroTzContract.methods
        .mint(conf.oussAddress, "50")
        .send();

      await euroTzMint.confirmation();

      const euroTzMintHash = euroTzMint.hash;

      console.log("euroTzMintHash : ", euroTzMintHash);

      expect(euroTzMintHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
