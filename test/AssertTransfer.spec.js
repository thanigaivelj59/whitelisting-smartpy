import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("Whitelist Smart Contract: AssertTransfer Entrypoint tests", function () {
  let euroTzContract;
  let whitelistContract;

  before(async function () {
    euroTzContract = await Tezos.contract.at(conf.euroTzContractAddress);
    whitelistContract = await Tezos.contract.at(conf.whitelistContractAddress);
  });

  /** ***************** Test trasnfer from Issuer to standard user **************** */

  it("Assert Trasnfer from Issuer to unexistant user - Issuer tries to transfer 9 euroTz to Fred / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await euroTzContract.methods
        .transfer("9", conf.adminAddress, conf.fredAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin adds Fred in whitelst contract", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const addUserOp = await whitelistContract.methods
        .addUser(conf.fredAddress, "2")
        .send();

      await addUserOp.confirmation();

      const addUserOpHash = addUserOp.hash;

      console.log("addUserOpHash : ", addUserOpHash);

      expect(addUserOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert Trasnfer from Issuer to existant user with whitelistID don't refer to an existing whitelist - Issuer tries to transfer 9 euroTz to Fred / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await euroTzContract.methods
        .transfer("9", conf.adminAddress, conf.fredAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Fred's whitelist outbound initally restricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("2", [], false)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert Trasnfer from Issuer to existant, restricted user - Issuer tries to transfer 9 euroTz to Fred / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await euroTzContract.methods
        .transfer("9", conf.adminAddress, conf.fredAddress)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Fred's whitelist outbound unrestricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("2", [], true)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert Trasnfer from Issuer to existant, unrestricted user - Issuer transfers 9 euroTz to Fred / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const euroTzTrasnferOp = await euroTzContract.methods
        .transfer("9", conf.adminAddress, conf.fredAddress)
        .send();

      await euroTzTrasnferOp.confirmation();

      const euroTzTrasnferOpHash = euroTzTrasnferOp.hash;

      console.log("euroTzTrasnferOpHash : ", euroTzTrasnferOpHash);

      expect(euroTzTrasnferOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  /** ***************** Test trasnfer between two standard users **************** */

  it("Assert Trasnfer from existant to an unexistant users - Fred tries to transfer 3 euroTz to Safwen / Should fail", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await euroTzContract.methods
        .transfer("3", conf.fredAddress, conf.safwen.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin adds Safwen in whitelst contract", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const addUserOp = await whitelistContract.methods
        .addUser(conf.safwen.pkh, "6")
        .send();

      await addUserOp.confirmation();

      const addUserOpHash = addUserOp.hash;

      console.log("addUserOpHash : ", addUserOpHash);

      expect(addUserOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert Trasnfer between: Two existant users with sender's whitelistID don't refer to an existing whitelist - Fred tries to transfer 3 euroTz to Safwen / Should fail", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await euroTzContract.methods
        .transfer("3", conf.fredAddress, conf.safwen.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Safwen's whitelist outbound unrestricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("6", [], true)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Fred's whitelist outbound restricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("2", [], false)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert Trasnfer between: Two existant users while sender is restricted -  Fred tries to transfer 3 euroTz to Safwen / Should fail", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await euroTzContract.methods
        .transfer("3", conf.fredAddress, conf.safwen.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Fred's whitelist outbound unrestricted and contain Safwen", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("2", ["6"], true)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert Trasnfer between: Two existants users, sender and receiver are unrestricted, receiver's whitelistID is in the sender's whitelist - Fred transfers 3 euroTz to Safwen / Should succeed", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      const euroTzTrasnferOp = await euroTzContract.methods
        .transfer("3", conf.fredAddress, conf.safwen.pkh)
        .send();

      await euroTzTrasnferOp.confirmation();

      const euroTzTrasnferOpHash = euroTzTrasnferOp.hash;

      console.log("euroTzTrasnferOpHash : ", euroTzTrasnferOpHash);

      expect(euroTzTrasnferOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
