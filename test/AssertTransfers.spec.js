import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("Whitelist Smart Contract: AssertTrasnfers Operation Testing", function () {
  let whitelistContract;

  before(async function () {
    whitelistContract = await Tezos.contract.at(conf.whitelistContractAddress);
    console.log("------------------------------------------------------");
    console.log("** Add User and set his whitelists outbounds. **");
    Tezos.importKey(conf.adminSecretKey);
    try {
      const addUserOp = await whitelistContract.methods
        .addUser(conf.thib.pkh, "3")
        .send();

      await addUserOp.confirmation();

      const addUserOpHash = addUserOp.hash;

      console.log("addUserOpHash : ", addUserOpHash);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setWhitelistOutbound("3", [], true)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }

    try {
      const addUserOp = await whitelistContract.methods
        .addUser(conf.bob.pkh, "6")
        .send();

      await addUserOp.confirmation();

      const addUserOpHash = addUserOp.hash;

      console.log("addUserOpHash : ", addUserOpHash);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setWhitelistOutbound("6", [], true)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }

    console.log("------------------------------------------------------");
  });

  it("Assert list of trasnfers with one unexistant sender / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .assertTransfers([
          { from_: conf.adminAddress, to_: conf.bob.pkh },
          { from_: conf.daly.pkh, to_: conf.thib.pkh },
        ])
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert list of transfers with one unexistant receiver / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .assertTransfers([
          { from_: conf.adminAddress, to_: conf.bob.pkh },
          { from_: conf.thib.pkh, to_: conf.daly.pkh },
        ])
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin adds Daly to users big_map in whitelst contract", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const addUserOp = await whitelistContract.methods
        .addUser(conf.daly.pkh, "5")
        .send();

      await addUserOp.confirmation();

      const addUserOpHash = addUserOp.hash;

      console.log("addUserOpHash : ", addUserOpHash);

      expect(addUserOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert list of transfers with one sender's whitelistID don't refer to an existing whitelist / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .assertTransfers([
          { from_: conf.adminAddress, to_: conf.bob.pkh },
          { from_: conf.daly.pkh, to_: conf.thib.pkh },
        ])
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Daly's whitelist outbound restricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("5", [], false)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert list of trasnfers with one restricted sender & restricted receiver / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .assertTransfers([
          { from_: conf.daly.pkh, to_: conf.bob.pkh },
          { from_: conf.thib.pkh, to_: conf.daly.pkh },
        ])
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Daly's whitelist outbound unrestricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("5", [], true)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert list of trasnfers with one transfer where the receiver's whitelistID is not in the sender's whitelist / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .assertTransfers([
          { from_: conf.adminAddress, to_: conf.bob.pkh },
          { from_: conf.daly.pkh, to_: conf.thib.pkh },
        ])
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin adds Thib to Daly's whitelist", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("5", ["3"], true)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert list of transfers: all senders & receivers are existant and unrestricted, all receivers are whitelisted in the senders' whitelists / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .assertTransfers([
          { from_: conf.adminAddress, to_: conf.bob.pkh },
          { from_: conf.daly.pkh, to_: conf.thib.pkh},
        ])
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
