import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("Whitelist Smart Contract: SetAdmin Entrypoint tests", function () {
  let whitelistContract;
  let whitelistStorage;

  before(async function () {
    whitelistContract = await Tezos.contract.at(conf.whitelistContractAddress);
    whitelistStorage = await whitelistContract.storage();
    const initialAdmin = whitelistStorage.admin;
    console.log("initialAdmin: ", initialAdmin);
    console.log("------------------------------------------------------------");
  });

  it("Update admin as non-admin / Should fail", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      await whitelistContract.methods.setAdmin(conf.fredAddress).send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Update admin as admin / Should succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setAdmin(conf.fredAddress)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);

      whitelistStorage = await whitelistContract.storage();
      const newAdmin = whitelistStorage.admin;
      console.log("Admin from storage: ", newAdmin);

      expect(whitelistContractOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Reset old admin as admin / Should succeed", async function () {
    Tezos.importKey(conf.fredSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setAdmin(conf.adminAddress)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);

      whitelistStorage = await whitelistContract.storage();
      const newAdmin = whitelistStorage.admin;
      console.log("Admin from storage: ", newAdmin);

      expect(whitelistContractOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
