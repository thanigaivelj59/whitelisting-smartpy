import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("Whitelist Smart Contract: AssertReceivers Entrypoint tests", function () {
  let whitelistContract;

  before(async function () {
    whitelistContract = await Tezos.contract.at(conf.whitelistContractAddress);
    console.log("------------------------------------------------------");
    console.log("** Add User and set his whitelists outbounds. **");
    Tezos.importKey(conf.adminSecretKey);
    try {
      const addUserOp = await whitelistContract.methods
        .addUser(conf.alice.pkh, "7")
        .send();

      await addUserOp.confirmation();

      const addUserOpHash = addUserOp.hash;

      console.log("addUserOpHash : ", addUserOpHash);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setWhitelistOutbound("7", [], true)
        .send();

      await whitelistContractOp.confirmation();

      const whitelistContractOpHash = whitelistContractOp.hash;

      console.log("whitelistContractOpHash : ", whitelistContractOpHash);
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
    console.log("------------------------------------------------------");
  });

  it("Assert receivers with one unexistant user / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .assertReceivers([
          conf.alice.pkh,
          conf.adminAddress,
          conf.khaledAddress,
        ])
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin adds Khaled in whitelst contract", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const addUserOp = await whitelistContract.methods
        .addUser(conf.khaledAddress, "4")
        .send();

      await addUserOp.confirmation();

      const addUserOpHash = addUserOp.hash;

      console.log("addUserOpHash : ", addUserOpHash);

      expect(addUserOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert receivers with one user's whitelistID don't refer to an existing whitelist / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .assertReceivers([
          conf.alice.pkh,
          conf.adminAddress,
          conf.khaledAddress,
        ])
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Khaled's whitelist outbound restricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("4", [], false)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert receivers with one user's whitelist restricted / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .assertReceivers([
          conf.alice.pkh,
          conf.adminAddress,
          conf.khaledAddress,
        ])
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Admin sets Khaled's whitelist outbound unrestricted", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .setWhitelistOutbound("4", [], true)
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Assert receivers: all users are existant and unrestricted / Sould succeed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const setWhitelistOutboundOp = await whitelistContract.methods
        .assertReceivers([
          conf.alice.pkh,
          conf.adminAddress,
          conf.khaledAddress,
        ])
        .send();

      await setWhitelistOutboundOp.confirmation();

      const setWhitelistOutboundOpHash = setWhitelistOutboundOp.hash;

      console.log("setWhitelistOutboundOpHash : ", setWhitelistOutboundOpHash);

      expect(setWhitelistOutboundOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
