import { Tezos, TezosOperationError } from "@taquito/taquito";
import { expect } from "chai";
import { BigNumber } from "bignumber.js";

import conf from "../conf/conf";

Tezos.setProvider({ rpc: conf.remoteNodeRPC });

describe("Whitelist Smart Contract: Informative Entrypoints tests", function () {
  let whitelistContract;
  let viewAdminContract;
  let viewIssuerContract;
  let viewWhitelistDetailsContract;
  let viewUserWhitelistIDContract;

  before(async function () {
    whitelistContract = await Tezos.contract.at(conf.whitelistContractAddress);
    viewAdminContract = await Tezos.contract.at(conf.viewAdminAddress);
    viewIssuerContract = await Tezos.contract.at(conf.viewIssuerAddress);
    viewWhitelistDetailsContract = await Tezos.contract.at(
      conf.viewWhitelistDetailsAddress
    );
    viewUserWhitelistIDContract = await Tezos.contract.at(
      conf.viewUserWhitelistAddress
    );
  });

  it("Get Admin Address / Should suucceed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const viewOp = await whitelistContract.methods
        .getAdmin(conf.viewAdminAddress)
        .send();

      await viewOp.confirmation();

      const viewOpHash = viewOp.hash;

      console.log("viewOpHash : ", viewOpHash);

      const viewAdminStorage = await viewAdminContract.storage();

      console.log("Admin from view contract storage: ", viewAdminStorage);

      expect(viewOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Get Issuer Address / Should suucceed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const viewOp = await whitelistContract.methods
        .getIssuer(conf.viewIssuerAddress)
        .send();

      await viewOp.confirmation();

      const viewOpHash = viewOp.hash;

      console.log("viewOpHash : ", viewOpHash);

      const viewIssuerStorage = await viewIssuerContract.storage();

      console.log("Issuer from view contract storage: ", viewIssuerStorage);

      expect(viewOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Get WhitelistDetails / Should suucceed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .setWhitelistOutbound("444", [], true)
        .send();

      await whitelistContractOp.confirmation();

      const viewOp = await whitelistContract.methods
        .getWhitelist(conf.viewWhitelistDetailsAddress, "444")
        .send();

      await viewOp.confirmation();

      const viewOpHash = viewOp.hash;

      console.log("viewOpHash : ", viewOpHash);

      const viewWhitelistDetailsStorage = await viewWhitelistDetailsContract.storage();

      console.log(
        "Whitelist details from view contract storage: ",
        viewWhitelistDetailsStorage
      );

      expect(viewOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Get User whitelist ID / Should suucceed", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      const whitelistContractOp = await whitelistContract.methods
        .addUser("tz1XsKQ8d8N1wYNWGTUBq6fX1bggGzQNRTh6", "444")
        .send();

      await whitelistContractOp.confirmation();

      const viewOp = await whitelistContract.methods
        .getUser(
          conf.viewUserWhitelistAddress,
          "tz1XsKQ8d8N1wYNWGTUBq6fX1bggGzQNRTh6"
        )
        .send();

      await viewOp.confirmation();

      const viewOpHash = viewOp.hash;

      console.log("viewOpHash : ", viewOpHash);

      const viewUserWhitelistStorage = await viewUserWhitelistIDContract.storage();

      console.log(
        "whiteListID from view contract storage: ",
        new BigNumber(viewUserWhitelistStorage).toNumber()
      );

      expect(viewOpHash).to.be.a("string");
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Get unexistant WhitelistDetails / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .getWhitelist(conf.viewWhitelistDetailsAddress, "56")
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });

  it("Get unexistant User / Should fail", async function () {
    Tezos.importKey(conf.adminSecretKey);

    try {
      await whitelistContract.methods
        .getUser(conf.viewUserWhitelistAddress, conf.bob.pkh)
        .send();
    } catch (e) {
      if (e instanceof TezosOperationError) {
        console.log("MESSAGE STATEMENT: ", e.errors[1].with.string);
      } else {
        console.log("PublicNode Error ", e);
      }
    }
  });
});
