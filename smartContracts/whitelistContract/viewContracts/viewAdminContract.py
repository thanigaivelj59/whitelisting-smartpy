import smartpy as sp

class ViewAdminContract(sp.Contract):
    def __init__(self):
        self.init(admin = sp.none)
        
    @sp.entry_point
    def setAdmin(self, admin):
        sp.set_type(admin, sp.TAddress)
        self.data.admin = sp.some(admin)

if "templates" not in __name__:
    @sp.add_test(name = "viewAdminContract")
    def test():

        viewAdminContract = ViewAdminContract()
        
        scenario = sp.test_scenario()
        
        scenario.h1("viewAdminContract tests")
        
        scenario += viewAdminContract
        
        
        
        
        