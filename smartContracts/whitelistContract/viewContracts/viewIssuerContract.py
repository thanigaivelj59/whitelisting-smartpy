import smartpy as sp

class ViewIssuerContract(sp.Contract):
    def __init__(self):
        self.init(issuer = sp.none)
        
    @sp.entry_point
    def setIssuer(self, issuer):
        sp.set_type(issuer, sp.TAddress)
        self.data.issuer = sp.some(issuer)

if "templates" not in __name__:
    @sp.add_test(name = "viewIssuerContract")
    def test():

        viewIssuerContract = ViewIssuerContract()
        
        scenario = sp.test_scenario()
        
        scenario.h1("viewIssuerContract tests")
        
        scenario += viewIssuerContract
        
        
        
        
        