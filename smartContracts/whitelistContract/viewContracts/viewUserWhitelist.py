import smartpy as sp

class ViewUserWhitelistContract(sp.Contract):
    def __init__(self):
        self.init(whiteListID = sp.none)
        
    @sp.entry_point
    def setUser(self, contractAddress, whiteListID):
        sp.set_type(whiteListID, sp.TNat)
        self.data.whiteListID = sp.some(whiteListID)

if "templates" not in __name__:
    @sp.add_test(name = "viewUserWhitelistContract")
    def test():

        viewUserWhitelistContract = ViewUserWhitelistContract()
        
        scenario = sp.test_scenario()
        
        scenario.h1("viewUserWhitelistContract tests")
        
        scenario += viewUserWhitelistContract
        
        
        
        
        