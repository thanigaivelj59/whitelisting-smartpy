import smartpy as sp

class ViewWhitelistDetailsContract(sp.Contract):
    def __init__(self):
        self.init(whitelistDetails = sp.none)
        
    @sp.entry_point
    def setWhitelist(self, whitelistDetails):
        sp.set_type(
            whitelistDetails, 
            sp.TRecord(
                unrestricted = sp.TBool, 
                allowed_whitelists = sp.TSet(t = sp.TNat))
                )
                
        self.data.whitelistDetails = sp.some(whitelistDetails)

if "templates" not in __name__:
    @sp.add_test(name = "viewWhitelistDetailsContract")
    def test():

        viewWhitelistDetailsContract = ViewWhitelistDetailsContract()
        
        scenario = sp.test_scenario()
        
        scenario.h1("viewWhitelistDetailsContract tests")
        
        scenario += viewWhitelistDetailsContract
        
        
        
        
        