export default [
  {
    prim: "storage",
    args: [
      {
        prim: "pair",
        args: [
          {
            prim: "pair",
            args: [
              { prim: "address", annots: ["%admin"] },
              { prim: "address", annots: ["%issuer"] },
            ],
          },
          {
            prim: "pair",
            args: [
              {
                prim: "big_map",
                args: [{ prim: "address" }, { prim: "nat" }],
                annots: ["%users"],
              },
              {
                prim: "big_map",
                args: [
                  { prim: "nat" },
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "set",
                        args: [{ prim: "nat" }],
                        annots: ["%allowed_whitelists"],
                      },
                      { prim: "bool", annots: ["%unrestricted"] },
                    ],
                  },
                ],
                annots: ["%whitelists"],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "parameter",
    args: [
      {
        prim: "or",
        args: [
          {
            prim: "or",
            args: [
              {
                prim: "or",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%new_user"] },
                      {
                        prim: "option",
                        args: [{ prim: "nat" }],
                        annots: ["%new_user_whitelist"],
                      },
                    ],
                    annots: ["%addUser"],
                  },
                  {
                    prim: "or",
                    args: [
                      { prim: "address", annots: ["%assertReceiver"] },
                      {
                        prim: "list",
                        args: [{ prim: "address" }],
                        annots: ["%assertReceivers"],
                      },
                    ],
                  },
                ],
              },
              {
                prim: "or",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%from_"] },
                      { prim: "address", annots: ["%to_"] },
                    ],
                    annots: ["%assertTransfer"],
                  },
                  {
                    prim: "or",
                    args: [
                      {
                        prim: "list",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "address", annots: ["%from_"] },
                              { prim: "address", annots: ["%to_"] },
                            ],
                          },
                        ],
                        annots: ["%assertTransfers"],
                      },
                      { prim: "address", annots: ["%getAdmin"] },
                    ],
                  },
                ],
              },
            ],
          },
          {
            prim: "or",
            args: [
              {
                prim: "or",
                args: [
                  { prim: "address", annots: ["%getIssuer"] },
                  {
                    prim: "or",
                    args: [
                      {
                        prim: "pair",
                        args: [
                          { prim: "address", annots: ["%contractAddress"] },
                          { prim: "address", annots: ["%user"] },
                        ],
                        annots: ["%getUser"],
                      },
                      {
                        prim: "pair",
                        args: [
                          { prim: "address", annots: ["%contractAddress"] },
                          { prim: "nat", annots: ["%whitelistID"] },
                        ],
                        annots: ["%getWhitelist"],
                      },
                    ],
                  },
                ],
              },
              {
                prim: "or",
                args: [
                  { prim: "address", annots: ["%setAdmin"] },
                  {
                    prim: "or",
                    args: [
                      { prim: "address", annots: ["%setIssuer"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "nat", annots: ["%new_id_whitelist"] },
                          {
                            prim: "option",
                            args: [
                              {
                                prim: "pair",
                                args: [
                                  {
                                    prim: "set",
                                    args: [{ prim: "nat" }],
                                    annots: ["%allowed_whitelists"],
                                  },
                                  { prim: "bool", annots: ["%unrestricted"] },
                                ],
                              },
                            ],
                            annots: ["%new_outbound_whitelists"],
                          },
                        ],
                        annots: ["%setWhitelistOutbound"],
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "code",
    args: [
      [
        { prim: "DUP" },
        { prim: "CDR" },
        { prim: "SWAP" },
        { prim: "CAR" },
        {
          prim: "IF_LEFT",
          args: [
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    [
                      {
                        prim: "IF_LEFT",
                        args: [
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "only admin may update" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "COMPARE" },
                              { prim: "NEQ" },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "issuer is not a user" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "bool" },
                                        { prim: "False" },
                                      ],
                                    },
                                  ],
                                  [
                                    [
                                      { prim: "DROP" },
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "bool" },
                                          { prim: "True" },
                                        ],
                                      },
                                    ],
                                  ],
                                ],
                              },
                              {
                                prim: "IF",
                                args: [
                                  [
                                    [
                                      { prim: "SWAP" },
                                      { prim: "DUP" },
                                      { prim: "CAR" },
                                      { prim: "SWAP" },
                                      { prim: "CDR" },
                                      { prim: "DUP" },
                                      { prim: "CDR" },
                                      { prim: "SWAP" },
                                      { prim: "CAR" },
                                      { prim: "DIG", args: [{ int: "3" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "4" }] },
                                      { prim: "CDR" },
                                      {
                                        prim: "IF_NONE",
                                        args: [
                                          [
                                            [
                                              {
                                                prim: "PUSH",
                                                args: [
                                                  { prim: "unit" },
                                                  { prim: "Unit" },
                                                ],
                                              },
                                              { prim: "FAILWITH" },
                                            ],
                                          ],
                                          [[]],
                                        ],
                                      },
                                      { prim: "SOME" },
                                      { prim: "DIG", args: [{ int: "4" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "5" }] },
                                      { prim: "CAR" },
                                      { prim: "UPDATE" },
                                      { prim: "PAIR" },
                                      { prim: "SWAP" },
                                      { prim: "PAIR" },
                                      { prim: "SWAP" },
                                    ],
                                  ],
                                  [
                                    [
                                      { prim: "SWAP" },
                                      { prim: "DUP" },
                                      { prim: "CAR" },
                                      { prim: "SWAP" },
                                      { prim: "CDR" },
                                      { prim: "DUP" },
                                      { prim: "CDR" },
                                      { prim: "SWAP" },
                                      { prim: "CAR" },
                                      { prim: "NONE", args: [{ prim: "nat" }] },
                                      { prim: "DIG", args: [{ int: "4" }] },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "5" }] },
                                      { prim: "CAR" },
                                      { prim: "UPDATE" },
                                      { prim: "PAIR" },
                                      { prim: "SWAP" },
                                      { prim: "PAIR" },
                                      { prim: "SWAP" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "DROP" },
                            ],
                          ],
                          [
                            {
                              prim: "IF_LEFT",
                              args: [
                                [
                                  [
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CAR" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "COMPARE" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            { prim: "SWAP" },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            { prim: "SWAP" },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "MEM" },
                                            {
                                              prim: "IF",
                                              args: [
                                                [[]],
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "user not on a whitelist",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                              ],
                                            },
                                            { prim: "SWAP" },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "3" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "3" }],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:251",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "MEM" },
                                            {
                                              prim: "IF",
                                              args: [
                                                [[]],
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "whitelist does not exist",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                              ],
                                            },
                                            { prim: "SWAP" },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CDR" },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "3" }],
                                            },
                                            { prim: "CDR" },
                                            { prim: "CAR" },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "2" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "3" }],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "Get-item:251",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "GET" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string: "Get-item:40",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [],
                                              ],
                                            },
                                            { prim: "CDR" },
                                            {
                                              prim: "IF",
                                              args: [
                                                [[]],
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "string" },
                                                        {
                                                          string:
                                                            "outbound restricted",
                                                        },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                              ],
                                            },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "DROP" },
                                  ],
                                ],
                                [
                                  [
                                    { prim: "DUP" },
                                    {
                                      prim: "ITER",
                                      args: [
                                        [
                                          { prim: "DIG", args: [{ int: "2" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "3" }] },
                                          { prim: "CAR" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "2" }] },
                                          { prim: "COMPARE" },
                                          { prim: "EQ" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [[]],
                                              [
                                                [
                                                  {
                                                    prim: "DIG",
                                                    args: [{ int: "2" }],
                                                  },
                                                  { prim: "DUP" },
                                                  {
                                                    prim: "DUG",
                                                    args: [{ int: "3" }],
                                                  },
                                                  { prim: "CDR" },
                                                  { prim: "CAR" },
                                                  { prim: "SWAP" },
                                                  { prim: "DUP" },
                                                  {
                                                    prim: "DUG",
                                                    args: [{ int: "2" }],
                                                  },
                                                  { prim: "MEM" },
                                                  {
                                                    prim: "IF",
                                                    args: [
                                                      [[]],
                                                      [
                                                        [
                                                          {
                                                            prim: "PUSH",
                                                            args: [
                                                              {
                                                                prim: "string",
                                                              },
                                                              {
                                                                string:
                                                                  "user not on a whitelist",
                                                              },
                                                            ],
                                                          },
                                                          { prim: "FAILWITH" },
                                                        ],
                                                      ],
                                                    ],
                                                  },
                                                  {
                                                    prim: "DIG",
                                                    args: [{ int: "2" }],
                                                  },
                                                  { prim: "DUP" },
                                                  {
                                                    prim: "DUG",
                                                    args: [{ int: "3" }],
                                                  },
                                                  { prim: "CDR" },
                                                  { prim: "CDR" },
                                                  {
                                                    prim: "DIG",
                                                    args: [{ int: "3" }],
                                                  },
                                                  { prim: "DUP" },
                                                  {
                                                    prim: "DUG",
                                                    args: [{ int: "4" }],
                                                  },
                                                  { prim: "CDR" },
                                                  { prim: "CAR" },
                                                  {
                                                    prim: "DIG",
                                                    args: [{ int: "2" }],
                                                  },
                                                  { prim: "DUP" },
                                                  {
                                                    prim: "DUG",
                                                    args: [{ int: "3" }],
                                                  },
                                                  { prim: "GET" },
                                                  {
                                                    prim: "IF_NONE",
                                                    args: [
                                                      [
                                                        [
                                                          {
                                                            prim: "PUSH",
                                                            args: [
                                                              {
                                                                prim: "string",
                                                              },
                                                              {
                                                                string:
                                                                  "Get-item:258",
                                                              },
                                                            ],
                                                          },
                                                          { prim: "FAILWITH" },
                                                        ],
                                                      ],
                                                      [],
                                                    ],
                                                  },
                                                  { prim: "MEM" },
                                                  {
                                                    prim: "IF",
                                                    args: [
                                                      [[]],
                                                      [
                                                        [
                                                          {
                                                            prim: "PUSH",
                                                            args: [
                                                              {
                                                                prim: "string",
                                                              },
                                                              {
                                                                string:
                                                                  "whitelist does not exist",
                                                              },
                                                            ],
                                                          },
                                                          { prim: "FAILWITH" },
                                                        ],
                                                      ],
                                                    ],
                                                  },
                                                  {
                                                    prim: "DIG",
                                                    args: [{ int: "2" }],
                                                  },
                                                  { prim: "DUP" },
                                                  {
                                                    prim: "DUG",
                                                    args: [{ int: "3" }],
                                                  },
                                                  { prim: "CDR" },
                                                  { prim: "CDR" },
                                                  {
                                                    prim: "DIG",
                                                    args: [{ int: "3" }],
                                                  },
                                                  { prim: "DUP" },
                                                  {
                                                    prim: "DUG",
                                                    args: [{ int: "4" }],
                                                  },
                                                  { prim: "CDR" },
                                                  { prim: "CAR" },
                                                  {
                                                    prim: "DIG",
                                                    args: [{ int: "2" }],
                                                  },
                                                  { prim: "DUP" },
                                                  {
                                                    prim: "DUG",
                                                    args: [{ int: "3" }],
                                                  },
                                                  { prim: "GET" },
                                                  {
                                                    prim: "IF_NONE",
                                                    args: [
                                                      [
                                                        [
                                                          {
                                                            prim: "PUSH",
                                                            args: [
                                                              {
                                                                prim: "string",
                                                              },
                                                              {
                                                                string:
                                                                  "Get-item:258",
                                                              },
                                                            ],
                                                          },
                                                          { prim: "FAILWITH" },
                                                        ],
                                                      ],
                                                      [],
                                                    ],
                                                  },
                                                  { prim: "GET" },
                                                  {
                                                    prim: "IF_NONE",
                                                    args: [
                                                      [
                                                        [
                                                          {
                                                            prim: "PUSH",
                                                            args: [
                                                              {
                                                                prim: "string",
                                                              },
                                                              {
                                                                string:
                                                                  "Get-item:40",
                                                              },
                                                            ],
                                                          },
                                                          { prim: "FAILWITH" },
                                                        ],
                                                      ],
                                                      [],
                                                    ],
                                                  },
                                                  { prim: "CDR" },
                                                  {
                                                    prim: "IF",
                                                    args: [
                                                      [[]],
                                                      [
                                                        [
                                                          {
                                                            prim: "PUSH",
                                                            args: [
                                                              {
                                                                prim: "string",
                                                              },
                                                              {
                                                                string:
                                                                  "outbound restricted",
                                                              },
                                                            ],
                                                          },
                                                          { prim: "FAILWITH" },
                                                        ],
                                                      ],
                                                    ],
                                                  },
                                                ],
                                              ],
                                            ],
                                          },
                                          { prim: "DROP" },
                                        ],
                                      ],
                                    },
                                    { prim: "DROP" },
                                  ],
                                ],
                              ],
                            },
                          ],
                        ],
                      },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                    ],
                  ],
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CAR" },
                            { prim: "COMPARE" },
                            { prim: "EQ" },
                            {
                              prim: "IF",
                              args: [
                                [
                                  [
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "MEM" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "user not on a whitelist",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:76" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "MEM" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "whitelist does not exist",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:76" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:40" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CDR" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string: "outbound restricted",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                  ],
                                ],
                                [
                                  [
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CAR" },
                                    { prim: "MEM" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "user not on a whitelist",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "MEM" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "user not on a whitelist",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CAR" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:75" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "MEM" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "whitelist does not exist",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CAR" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:75" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:40" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CDR" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string: "outbound restricted",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:76" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "MEM" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "whitelist does not exist",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:76" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:40" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CDR" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string: "outbound restricted",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CDR" },
                                    { prim: "CDR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CAR" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:75" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:40" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "CDR" },
                                    { prim: "GET" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                { string: "Get-item:76" },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                        [],
                                      ],
                                    },
                                    { prim: "MEM" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "outbound not whitelisted",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                  ],
                                ],
                              ],
                            },
                            { prim: "DROP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                          ],
                        ],
                        [
                          {
                            prim: "IF_LEFT",
                            args: [
                              [
                                [
                                  { prim: "DUP" },
                                  {
                                    prim: "ITER",
                                    args: [
                                      [
                                        { prim: "DIG", args: [{ int: "2" }] },
                                        { prim: "DUP" },
                                        { prim: "DUG", args: [{ int: "3" }] },
                                        { prim: "CAR" },
                                        { prim: "CDR" },
                                        { prim: "SWAP" },
                                        { prim: "DUP" },
                                        { prim: "DUG", args: [{ int: "2" }] },
                                        { prim: "CAR" },
                                        { prim: "COMPARE" },
                                        { prim: "EQ" },
                                        {
                                          prim: "IF",
                                          args: [
                                            [
                                              [
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                { prim: "SWAP" },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "MEM" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "user not on a whitelist",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "4" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:76",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "MEM" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "whitelist does not exist",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "4" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:76",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:40",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "CDR" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "outbound restricted",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                              ],
                                            ],
                                            [
                                              [
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                { prim: "SWAP" },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "CAR" },
                                                { prim: "MEM" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "user not on a whitelist",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                { prim: "SWAP" },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "MEM" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "user not on a whitelist",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "4" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CAR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:75",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "MEM" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "whitelist does not exist",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "4" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CAR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:75",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:40",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "CDR" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "outbound restricted",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "4" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:76",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "MEM" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "whitelist does not exist",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "4" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:76",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:40",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "CDR" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "outbound restricted",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CDR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "4" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CAR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:75",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:40",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "CAR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "4" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "CAR" },
                                                {
                                                  prim: "DIG",
                                                  args: [{ int: "2" }],
                                                },
                                                { prim: "DUP" },
                                                {
                                                  prim: "DUG",
                                                  args: [{ int: "3" }],
                                                },
                                                { prim: "CDR" },
                                                { prim: "GET" },
                                                {
                                                  prim: "IF_NONE",
                                                  args: [
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "Get-item:76",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                    [],
                                                  ],
                                                },
                                                { prim: "MEM" },
                                                {
                                                  prim: "IF",
                                                  args: [
                                                    [[]],
                                                    [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "string" },
                                                            {
                                                              string:
                                                                "outbound not whitelisted",
                                                            },
                                                          ],
                                                        },
                                                        { prim: "FAILWITH" },
                                                      ],
                                                    ],
                                                  ],
                                                },
                                              ],
                                            ],
                                          ],
                                        },
                                        { prim: "DROP" },
                                      ],
                                    ],
                                  },
                                  { prim: "DROP" },
                                  {
                                    prim: "NIL",
                                    args: [{ prim: "operation" }],
                                  },
                                ],
                              ],
                              [
                                [
                                  {
                                    prim: "CONTRACT",
                                    args: [{ prim: "address" }],
                                  },
                                  {
                                    prim: "NIL",
                                    args: [{ prim: "operation" }],
                                  },
                                  { prim: "SWAP" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "unit" },
                                              { prim: "Unit" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  {
                                    prim: "PUSH",
                                    args: [{ prim: "mutez" }, { int: "0" }],
                                  },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CAR" },
                                  { prim: "CAR" },
                                  { prim: "TRANSFER_TOKENS" },
                                  { prim: "CONS" },
                                ],
                              ],
                            ],
                          },
                        ],
                      ],
                    },
                  ],
                ],
              },
            ],
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            { prim: "CONTRACT", args: [{ prim: "address" }] },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "SWAP" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                          ],
                        ],
                        [
                          {
                            prim: "IF_LEFT",
                            args: [
                              [
                                [
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "CDR" },
                                  { prim: "MEM" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [[]],
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "user not found" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                    ],
                                  },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "CONTRACT", args: [{ prim: "nat" }] },
                                  {
                                    prim: "NIL",
                                    args: [{ prim: "operation" }],
                                  },
                                  { prim: "SWAP" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "unit" },
                                              { prim: "Unit" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  {
                                    prim: "PUSH",
                                    args: [{ prim: "mutez" }, { int: "0" }],
                                  },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "CDR" },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "Get-item:20" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "TRANSFER_TOKENS" },
                                  { prim: "CONS" },
                                ],
                              ],
                              [
                                [
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "CDR" },
                                  { prim: "MEM" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [[]],
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "whitelist not found" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                    ],
                                  },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  {
                                    prim: "CONTRACT",
                                    args: [
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "set",
                                            args: [{ prim: "nat" }],
                                            annots: ["%allowed_whitelists"],
                                          },
                                          {
                                            prim: "bool",
                                            annots: ["%unrestricted"],
                                          },
                                        ],
                                      },
                                    ],
                                  },
                                  {
                                    prim: "NIL",
                                    args: [{ prim: "operation" }],
                                  },
                                  { prim: "SWAP" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "unit" },
                                              { prim: "Unit" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  {
                                    prim: "PUSH",
                                    args: [{ prim: "mutez" }, { int: "0" }],
                                  },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "CDR" },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "Get-item:20" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "TRANSFER_TOKENS" },
                                  { prim: "CONS" },
                                ],
                              ],
                            ],
                          },
                        ],
                      ],
                    },
                  ],
                  [
                    [
                      {
                        prim: "IF_LEFT",
                        args: [
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "only admin may update" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                            ],
                          ],
                          [
                            {
                              prim: "IF_LEFT",
                              args: [
                                [
                                  [
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CAR" },
                                    { prim: "CAR" },
                                    { prim: "SENDER" },
                                    { prim: "COMPARE" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "only admin may update",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "CAR" },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "PAIR" },
                                  ],
                                ],
                                [
                                  [
                                    { prim: "SWAP" },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "2" }] },
                                    { prim: "CAR" },
                                    { prim: "CAR" },
                                    { prim: "SENDER" },
                                    { prim: "COMPARE" },
                                    { prim: "EQ" },
                                    {
                                      prim: "IF",
                                      args: [
                                        [[]],
                                        [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "string" },
                                                {
                                                  string:
                                                    "only admin may update",
                                                },
                                              ],
                                            },
                                            { prim: "FAILWITH" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    {
                                      prim: "IF_NONE",
                                      args: [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "bool" },
                                              { prim: "False" },
                                            ],
                                          },
                                        ],
                                        [
                                          [
                                            { prim: "DROP" },
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "bool" },
                                                { prim: "True" },
                                              ],
                                            },
                                          ],
                                        ],
                                      ],
                                    },
                                    {
                                      prim: "IF",
                                      args: [
                                        [
                                          [
                                            { prim: "SWAP" },
                                            { prim: "DUP" },
                                            { prim: "CAR" },
                                            { prim: "SWAP" },
                                            { prim: "CDR" },
                                            { prim: "DUP" },
                                            { prim: "CAR" },
                                            { prim: "SWAP" },
                                            { prim: "CDR" },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "3" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "4" }],
                                            },
                                            { prim: "CDR" },
                                            {
                                              prim: "IF_NONE",
                                              args: [
                                                [
                                                  [
                                                    {
                                                      prim: "PUSH",
                                                      args: [
                                                        { prim: "unit" },
                                                        { prim: "Unit" },
                                                      ],
                                                    },
                                                    { prim: "FAILWITH" },
                                                  ],
                                                ],
                                                [[]],
                                              ],
                                            },
                                            { prim: "SOME" },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "4" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "CAR" },
                                            { prim: "UPDATE" },
                                            { prim: "SWAP" },
                                            { prim: "PAIR" },
                                            { prim: "SWAP" },
                                            { prim: "PAIR" },
                                            { prim: "SWAP" },
                                          ],
                                        ],
                                        [
                                          [
                                            { prim: "SWAP" },
                                            { prim: "DUP" },
                                            { prim: "CAR" },
                                            { prim: "SWAP" },
                                            { prim: "CDR" },
                                            { prim: "DUP" },
                                            { prim: "CAR" },
                                            { prim: "SWAP" },
                                            { prim: "CDR" },
                                            {
                                              prim: "NONE",
                                              args: [
                                                {
                                                  prim: "pair",
                                                  args: [
                                                    {
                                                      prim: "set",
                                                      args: [{ prim: "nat" }],
                                                      annots: [
                                                        "%allowed_whitelists",
                                                      ],
                                                    },
                                                    {
                                                      prim: "bool",
                                                      annots: ["%unrestricted"],
                                                    },
                                                  ],
                                                },
                                              ],
                                            },
                                            {
                                              prim: "DIG",
                                              args: [{ int: "4" }],
                                            },
                                            { prim: "DUP" },
                                            {
                                              prim: "DUG",
                                              args: [{ int: "5" }],
                                            },
                                            { prim: "CAR" },
                                            { prim: "UPDATE" },
                                            { prim: "SWAP" },
                                            { prim: "PAIR" },
                                            { prim: "SWAP" },
                                            { prim: "PAIR" },
                                            { prim: "SWAP" },
                                          ],
                                        ],
                                      ],
                                    },
                                    { prim: "DROP" },
                                  ],
                                ],
                              ],
                            },
                          ],
                        ],
                      },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                    ],
                  ],
                ],
              },
            ],
          ],
        },
        { prim: "PAIR" },
      ],
    ],
  },
];
