export default [
  {
    prim: "storage",
    args: [
      {
        prim: "pair",
        args: [
          {
            prim: "pair",
            args: [
              { prim: "address", annots: ["%administrator"] },
              {
                prim: "big_map",
                args: [
                  { prim: "address" },
                  {
                    prim: "pair",
                    args: [
                      {
                        prim: "map",
                        args: [{ prim: "address" }, { prim: "int" }],
                        annots: ["%approvals"],
                      },
                      { prim: "int", annots: ["%balance"] },
                    ],
                  },
                ],
                annots: ["%balances"],
              },
            ],
          },
          {
            prim: "pair",
            args: [
              { prim: "bool", annots: ["%paused"] },
              {
                prim: "pair",
                args: [
                  { prim: "address", annots: ["%safelistAddress"] },
                  { prim: "int", annots: ["%totalSupply"] },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "parameter",
    args: [
      {
        prim: "or",
        args: [
          {
            prim: "or",
            args: [
              {
                prim: "or",
                args: [
                  {
                    prim: "pair",
                    args: [
                      { prim: "int", annots: ["%amount"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "address", annots: ["%f"] },
                          { prim: "address", annots: ["%t"] },
                        ],
                      },
                    ],
                    annots: ["%approve"],
                  },
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%address"] },
                      { prim: "int", annots: ["%amount"] },
                    ],
                    annots: ["%burn"],
                  },
                ],
              },
              {
                prim: "or",
                args: [
                  { prim: "address", annots: ["%getAdministrator"] },
                  {
                    prim: "or",
                    args: [
                      {
                        prim: "pair",
                        args: [
                          {
                            prim: "pair",
                            args: [
                              { prim: "address", annots: ["%owner"] },
                              { prim: "address", annots: ["%spender"] },
                            ],
                            annots: ["%arg"],
                          },
                          { prim: "address", annots: ["%target"] },
                        ],
                        annots: ["%getAllowance"],
                      },
                      {
                        prim: "pair",
                        args: [
                          { prim: "address", annots: ["%arg"] },
                          { prim: "address", annots: ["%target"] },
                        ],
                        annots: ["%getBalance"],
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            prim: "or",
            args: [
              {
                prim: "or",
                args: [
                  { prim: "address", annots: ["%getTotalSupply"] },
                  {
                    prim: "pair",
                    args: [
                      { prim: "address", annots: ["%address"] },
                      { prim: "int", annots: ["%amount"] },
                    ],
                    annots: ["%mint"],
                  },
                ],
              },
              {
                prim: "or",
                args: [
                  { prim: "address", annots: ["%setAdministrator"] },
                  {
                    prim: "or",
                    args: [
                      { prim: "bool", annots: ["%setPause"] },
                      {
                        prim: "pair",
                        args: [
                          { prim: "int", annots: ["%amount"] },
                          {
                            prim: "pair",
                            args: [
                              { prim: "address", annots: ["%f"] },
                              { prim: "address", annots: ["%t"] },
                            ],
                          },
                        ],
                        annots: ["%transfer"],
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  },
  {
    prim: "code",
    args: [
      [
        { prim: "DUP" },
        { prim: "CDR" },
        { prim: "SWAP" },
        { prim: "CAR" },
        {
          prim: "IF_LEFT",
          args: [
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    [
                      {
                        prim: "IF_LEFT",
                        args: [
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "bool" },
                                        { prim: "True" },
                                      ],
                                    },
                                  ],
                                  [
                                    [
                                      { prim: "SWAP" },
                                      { prim: "DUP" },
                                      { prim: "DUG", args: [{ int: "2" }] },
                                      { prim: "CDR" },
                                      { prim: "CAR" },
                                      {
                                        prim: "IF",
                                        args: [
                                          [
                                            {
                                              prim: "PUSH",
                                              args: [
                                                { prim: "bool" },
                                                { prim: "False" },
                                              ],
                                            },
                                          ],
                                          [
                                            [
                                              { prim: "DUP" },
                                              { prim: "CDR" },
                                              { prim: "CAR" },
                                              { prim: "SENDER" },
                                              { prim: "COMPARE" },
                                              { prim: "EQ" },
                                            ],
                                          ],
                                        ],
                                      },
                                    ],
                                  ],
                                ],
                              },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          {
                                            string:
                                              "WrongCondition: (sp.sender == self.data.administrator) | ((~ self.data.paused) & (params.f == sp.sender))",
                                          },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              {
                                prim: "PUSH",
                                args: [{ prim: "int" }, { int: "0" }],
                              },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:65" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [{ prim: "int" }, { int: "0" }],
                                    },
                                  ],
                                  [],
                                ],
                              },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "bool" },
                                        { prim: "True" },
                                      ],
                                    },
                                  ],
                                  [
                                    [
                                      { prim: "DUP" },
                                      { prim: "CAR" },
                                      {
                                        prim: "PUSH",
                                        args: [{ prim: "int" }, { int: "0" }],
                                      },
                                      { prim: "COMPARE" },
                                      { prim: "EQ" },
                                    ],
                                  ],
                                ],
                              },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          {
                                            string:
                                              "WrongCondition: (self.data.balances[params.f].approvals.get(params.t, 0) == 0) | (params.amount == 0)",
                                          },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "DIG", args: [{ int: "4" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "5" }] },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "set_in_top-any" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "6" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "7" }] },
                              { prim: "CAR" },
                              { prim: "SOME" },
                              { prim: "DIG", args: [{ int: "7" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "8" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "UPDATE" },
                              { prim: "PAIR" },
                              { prim: "SOME" },
                              { prim: "SWAP" },
                              { prim: "UPDATE" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "DROP", args: [{ int: "2" }] },
                            ],
                          ],
                          [
                            [
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "CAR" },
                              { prim: "CAR" },
                              { prim: "SENDER" },
                              { prim: "COMPARE" },
                              { prim: "EQ" },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          {
                                            string:
                                              "WrongCondition: sp.sender == self.data.administrator",
                                          },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "3" }] },
                              { prim: "CAR" },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:92" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "COMPARE" },
                              { prim: "GE" },
                              {
                                prim: "IF",
                                args: [
                                  [[]],
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          {
                                            string:
                                              "WrongCondition: self.data.balances[params.address].balance >= params.amount",
                                          },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                ],
                              },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "CDR" },
                              { prim: "SWAP" },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "DIG", args: [{ int: "4" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "5" }] },
                              { prim: "CAR" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "set_in_top-any" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "5" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "6" }] },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "7" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "8" }] },
                              { prim: "CAR" },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "7" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "8" }] },
                              { prim: "CAR" },
                              { prim: "GET" },
                              {
                                prim: "IF_NONE",
                                args: [
                                  [
                                    [
                                      {
                                        prim: "PUSH",
                                        args: [
                                          { prim: "string" },
                                          { string: "Get-item:92" },
                                        ],
                                      },
                                      { prim: "FAILWITH" },
                                    ],
                                  ],
                                  [],
                                ],
                              },
                              { prim: "CDR" },
                              { prim: "SUB" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SOME" },
                              { prim: "SWAP" },
                              { prim: "UPDATE" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "PAIR" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "SWAP" },
                              { prim: "DROP" },
                              { prim: "SWAP" },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "DUP" },
                              { prim: "CAR" },
                              { prim: "SWAP" },
                              { prim: "CDR" },
                              { prim: "CAR" },
                              { prim: "DIG", args: [{ int: "3" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "4" }] },
                              { prim: "CDR" },
                              { prim: "DIG", args: [{ int: "5" }] },
                              { prim: "DUP" },
                              { prim: "DUG", args: [{ int: "6" }] },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "CDR" },
                              { prim: "SUB" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "SWAP" },
                              { prim: "PAIR" },
                              { prim: "DUG", args: [{ int: "2" }] },
                              { prim: "DROP", args: [{ int: "2" }] },
                            ],
                          ],
                        ],
                      },
                      { prim: "NIL", args: [{ prim: "operation" }] },
                    ],
                  ],
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            { prim: "DUP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "SWAP" },
                            { prim: "CONTRACT", args: [{ prim: "address" }] },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                            { prim: "SWAP" },
                            { prim: "DROP" },
                          ],
                        ],
                        [
                          {
                            prim: "IF_LEFT",
                            args: [
                              [
                                [
                                  { prim: "DUP" },
                                  { prim: "CDR" },
                                  {
                                    prim: "NIL",
                                    args: [{ prim: "operation" }],
                                  },
                                  { prim: "SWAP" },
                                  { prim: "CONTRACT", args: [{ prim: "nat" }] },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "unit" },
                                              { prim: "Unit" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  {
                                    prim: "PUSH",
                                    args: [{ prim: "mutez" }, { int: "0" }],
                                  },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CAR" },
                                  { prim: "CAR" },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "Get-item:106" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "CAR" },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "Get-item:106" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "ISNAT" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "unit" },
                                              { prim: "Unit" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  { prim: "TRANSFER_TOKENS" },
                                  { prim: "CONS" },
                                  { prim: "SWAP" },
                                  { prim: "DROP" },
                                ],
                              ],
                              [
                                [
                                  { prim: "DUP" },
                                  { prim: "CDR" },
                                  {
                                    prim: "NIL",
                                    args: [{ prim: "operation" }],
                                  },
                                  { prim: "SWAP" },
                                  { prim: "CONTRACT", args: [{ prim: "nat" }] },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "unit" },
                                              { prim: "Unit" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  {
                                    prim: "PUSH",
                                    args: [{ prim: "mutez" }, { int: "0" }],
                                  },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CAR" },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "Get-item:102" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "CDR" },
                                  { prim: "ISNAT" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "unit" },
                                              { prim: "Unit" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  { prim: "TRANSFER_TOKENS" },
                                  { prim: "CONS" },
                                  { prim: "SWAP" },
                                  { prim: "DROP" },
                                ],
                              ],
                            ],
                          },
                        ],
                      ],
                    },
                  ],
                ],
              },
            ],
            [
              {
                prim: "IF_LEFT",
                args: [
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            { prim: "DUP" },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "SWAP" },
                            { prim: "CONTRACT", args: [{ prim: "nat" }] },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "ISNAT" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                            { prim: "SWAP" },
                            { prim: "DROP" },
                          ],
                        ],
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "SENDER" },
                            { prim: "COMPARE" },
                            { prim: "EQ" },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        {
                                          string:
                                            "WrongCondition: sp.sender == self.data.administrator",
                                        },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            {
                              prim: "CONTRACT",
                              args: [{ prim: "address" }],
                              annots: ["%assertReceiver"],
                            },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "unit" },
                                        { prim: "Unit" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [[]],
                              ],
                            },
                            {
                              prim: "PUSH",
                              args: [{ prim: "mutez" }, { int: "0" }],
                            },
                            { prim: "DIG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "4" }] },
                            { prim: "CAR" },
                            { prim: "TRANSFER_TOKENS" },
                            { prim: "CONS" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "CAR" },
                            { prim: "MEM" },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "DUP" },
                                    { prim: "CDR" },
                                    { prim: "SWAP" },
                                    { prim: "CAR" },
                                    { prim: "DUP" },
                                    { prim: "CAR" },
                                    { prim: "SWAP" },
                                    { prim: "CDR" },
                                    {
                                      prim: "PUSH",
                                      args: [
                                        {
                                          prim: "option",
                                          args: [
                                            {
                                              prim: "pair",
                                              args: [
                                                {
                                                  prim: "map",
                                                  args: [
                                                    { prim: "address" },
                                                    { prim: "int" },
                                                  ],
                                                  annots: ["%approvals"],
                                                },
                                                {
                                                  prim: "int",
                                                  annots: ["%balance"],
                                                },
                                              ],
                                            },
                                          ],
                                        },
                                        {
                                          prim: "Some",
                                          args: [
                                            {
                                              prim: "Pair",
                                              args: [[], { int: "0" }],
                                            },
                                          ],
                                        },
                                      ],
                                    },
                                    { prim: "DIG", args: [{ int: "5" }] },
                                    { prim: "DUP" },
                                    { prim: "DUG", args: [{ int: "6" }] },
                                    { prim: "CAR" },
                                    { prim: "UPDATE" },
                                    { prim: "SWAP" },
                                    { prim: "PAIR" },
                                    { prim: "PAIR" },
                                    { prim: "DUG", args: [{ int: "3" }] },
                                    { prim: "DIG", args: [{ int: "2" }] },
                                    { prim: "DROP" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "DIG", args: [{ int: "5" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "6" }] },
                            { prim: "CAR" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "set_in_top-any" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "6" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "7" }] },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "8" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "9" }] },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "8" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "9" }] },
                            { prim: "CAR" },
                            { prim: "GET" },
                            {
                              prim: "IF_NONE",
                              args: [
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        { string: "Get-item:84" },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                                [],
                              ],
                            },
                            { prim: "CDR" },
                            { prim: "ADD" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SOME" },
                            { prim: "SWAP" },
                            { prim: "UPDATE" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DROP" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "DUP" },
                            { prim: "CAR" },
                            { prim: "SWAP" },
                            { prim: "CDR" },
                            { prim: "CAR" },
                            { prim: "DIG", args: [{ int: "4" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "5" }] },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "6" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "7" }] },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "CDR" },
                            { prim: "ADD" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "SWAP" },
                            { prim: "PAIR" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DROP" },
                            { prim: "SWAP" },
                            { prim: "DROP" },
                          ],
                        ],
                      ],
                    },
                  ],
                  [
                    {
                      prim: "IF_LEFT",
                      args: [
                        [
                          [
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "CAR" },
                            { prim: "CAR" },
                            { prim: "SENDER" },
                            { prim: "COMPARE" },
                            { prim: "EQ" },
                            {
                              prim: "IF",
                              args: [
                                [[]],
                                [
                                  [
                                    {
                                      prim: "PUSH",
                                      args: [
                                        { prim: "string" },
                                        {
                                          string:
                                            "WrongCondition: sp.sender == self.data.administrator",
                                        },
                                      ],
                                    },
                                    { prim: "FAILWITH" },
                                  ],
                                ],
                              ],
                            },
                            { prim: "SWAP" },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "CDR" },
                            { prim: "SWAP" },
                            { prim: "CAR" },
                            { prim: "CDR" },
                            { prim: "DIG", args: [{ int: "2" }] },
                            { prim: "DUP" },
                            { prim: "DUG", args: [{ int: "3" }] },
                            { prim: "PAIR" },
                            { prim: "PAIR" },
                            { prim: "DUG", args: [{ int: "2" }] },
                            { prim: "DROP", args: [{ int: "2" }] },
                            { prim: "NIL", args: [{ prim: "operation" }] },
                          ],
                        ],
                        [
                          {
                            prim: "IF_LEFT",
                            args: [
                              [
                                [
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "CAR" },
                                  { prim: "CAR" },
                                  { prim: "SENDER" },
                                  { prim: "COMPARE" },
                                  { prim: "EQ" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [[]],
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              {
                                                string:
                                                  "WrongCondition: sp.sender == self.data.administrator",
                                              },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                    ],
                                  },
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "PAIR" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "DROP", args: [{ int: "2" }] },
                                  {
                                    prim: "NIL",
                                    args: [{ prim: "operation" }],
                                  },
                                ],
                              ],
                              [
                                [
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "CAR" },
                                  { prim: "CAR" },
                                  { prim: "SENDER" },
                                  { prim: "COMPARE" },
                                  { prim: "EQ" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "bool" },
                                            { prim: "True" },
                                          ],
                                        },
                                      ],
                                      [
                                        [
                                          { prim: "SWAP" },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "2" }] },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          {
                                            prim: "IF",
                                            args: [
                                              [
                                                {
                                                  prim: "PUSH",
                                                  args: [
                                                    { prim: "bool" },
                                                    { prim: "False" },
                                                  ],
                                                },
                                              ],
                                              [
                                                [
                                                  { prim: "DUP" },
                                                  { prim: "CDR" },
                                                  { prim: "CAR" },
                                                  { prim: "SENDER" },
                                                  { prim: "COMPARE" },
                                                  { prim: "EQ" },
                                                  {
                                                    prim: "IF",
                                                    args: [
                                                      [
                                                        {
                                                          prim: "PUSH",
                                                          args: [
                                                            { prim: "bool" },
                                                            { prim: "True" },
                                                          ],
                                                        },
                                                      ],
                                                      [
                                                        [
                                                          { prim: "DUP" },
                                                          { prim: "CAR" },
                                                          {
                                                            prim: "DIG",
                                                            args: [
                                                              { int: "2" },
                                                            ],
                                                          },
                                                          { prim: "DUP" },
                                                          {
                                                            prim: "DUG",
                                                            args: [
                                                              { int: "3" },
                                                            ],
                                                          },
                                                          { prim: "CAR" },
                                                          { prim: "CDR" },
                                                          {
                                                            prim: "DIG",
                                                            args: [
                                                              { int: "2" },
                                                            ],
                                                          },
                                                          { prim: "DUP" },
                                                          {
                                                            prim: "DUG",
                                                            args: [
                                                              { int: "3" },
                                                            ],
                                                          },
                                                          { prim: "CDR" },
                                                          { prim: "CAR" },
                                                          { prim: "GET" },
                                                          {
                                                            prim: "IF_NONE",
                                                            args: [
                                                              [
                                                                [
                                                                  {
                                                                    prim:
                                                                      "PUSH",
                                                                    args: [
                                                                      {
                                                                        prim:
                                                                          "string",
                                                                      },
                                                                      {
                                                                        string:
                                                                          "Get-item:51",
                                                                      },
                                                                    ],
                                                                  },
                                                                  {
                                                                    prim:
                                                                      "FAILWITH",
                                                                  },
                                                                ],
                                                              ],
                                                              [],
                                                            ],
                                                          },
                                                          { prim: "CAR" },
                                                          { prim: "SENDER" },
                                                          { prim: "GET" },
                                                          {
                                                            prim: "IF_NONE",
                                                            args: [
                                                              [
                                                                [
                                                                  {
                                                                    prim:
                                                                      "PUSH",
                                                                    args: [
                                                                      {
                                                                        prim:
                                                                          "string",
                                                                      },
                                                                      {
                                                                        string:
                                                                          "Get-item:-1",
                                                                      },
                                                                    ],
                                                                  },
                                                                  {
                                                                    prim:
                                                                      "FAILWITH",
                                                                  },
                                                                ],
                                                              ],
                                                              [],
                                                            ],
                                                          },
                                                          { prim: "COMPARE" },
                                                          { prim: "GE" },
                                                        ],
                                                      ],
                                                    ],
                                                  },
                                                ],
                                              ],
                                            ],
                                          },
                                        ],
                                      ],
                                    ],
                                  },
                                  {
                                    prim: "IF",
                                    args: [
                                      [[]],
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              {
                                                string:
                                                  "WrongCondition: (sp.sender == self.data.administrator) | ((~ self.data.paused) & ((params.f == sp.sender) | (self.data.balances[params.f].approvals[sp.sender] >= params.amount)))",
                                              },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                    ],
                                  },
                                  {
                                    prim: "NIL",
                                    args: [{ prim: "operation" }],
                                  },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  {
                                    prim: "CONTRACT",
                                    args: [
                                      {
                                        prim: "pair",
                                        args: [
                                          {
                                            prim: "address",
                                            annots: ["%from_"],
                                          },
                                          { prim: "address", annots: ["%to_"] },
                                        ],
                                      },
                                    ],
                                    annots: ["%assertTransfer"],
                                  },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "unit" },
                                              { prim: "Unit" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  {
                                    prim: "PUSH",
                                    args: [{ prim: "mutez" }, { int: "0" }],
                                  },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "4" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "5" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "PAIR", annots: ["%from_", "%to_"] },
                                  { prim: "TRANSFER_TOKENS" },
                                  { prim: "CONS" },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "MEM" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [[]],
                                      [
                                        [
                                          { prim: "DIG", args: [{ int: "2" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "3" }] },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          {
                                            prim: "PUSH",
                                            args: [
                                              {
                                                prim: "option",
                                                args: [
                                                  {
                                                    prim: "pair",
                                                    args: [
                                                      {
                                                        prim: "map",
                                                        args: [
                                                          { prim: "address" },
                                                          { prim: "int" },
                                                        ],
                                                        annots: ["%approvals"],
                                                      },
                                                      {
                                                        prim: "int",
                                                        annots: ["%balance"],
                                                      },
                                                    ],
                                                  },
                                                ],
                                              },
                                              {
                                                prim: "Some",
                                                args: [
                                                  {
                                                    prim: "Pair",
                                                    args: [[], { int: "0" }],
                                                  },
                                                ],
                                              },
                                            ],
                                          },
                                          { prim: "DIG", args: [{ int: "5" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "6" }] },
                                          { prim: "CDR" },
                                          { prim: "CDR" },
                                          { prim: "UPDATE" },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "PAIR" },
                                          { prim: "DUG", args: [{ int: "3" }] },
                                          { prim: "DIG", args: [{ int: "2" }] },
                                          { prim: "DROP" },
                                        ],
                                      ],
                                    ],
                                  },
                                  { prim: "SWAP" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "CAR" },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "4" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "Get-item:51" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "CDR" },
                                  { prim: "COMPARE" },
                                  { prim: "GE" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [[]],
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              {
                                                string:
                                                  "WrongCondition: self.data.balances[params.f].balance >= params.amount",
                                              },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                    ],
                                  },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "CDR" },
                                  { prim: "SWAP" },
                                  { prim: "CAR" },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "DIG", args: [{ int: "5" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "6" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "set_in_top-any" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "CAR" },
                                  { prim: "DIG", args: [{ int: "6" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "7" }] },
                                  { prim: "CAR" },
                                  { prim: "DIG", args: [{ int: "8" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "9" }] },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "8" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "9" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "Get-item:51" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "CDR" },
                                  { prim: "SUB" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SOME" },
                                  { prim: "SWAP" },
                                  { prim: "UPDATE" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "PAIR" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DROP" },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "DUP" },
                                  { prim: "CDR" },
                                  { prim: "SWAP" },
                                  { prim: "CAR" },
                                  { prim: "DUP" },
                                  { prim: "CAR" },
                                  { prim: "SWAP" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "DIG", args: [{ int: "5" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "6" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "2" }] },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "set_in_top-any" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "CAR" },
                                  { prim: "DIG", args: [{ int: "6" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "7" }] },
                                  { prim: "CAR" },
                                  { prim: "DIG", args: [{ int: "8" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "9" }] },
                                  { prim: "CAR" },
                                  { prim: "CDR" },
                                  { prim: "DIG", args: [{ int: "8" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "9" }] },
                                  { prim: "CDR" },
                                  { prim: "CDR" },
                                  { prim: "GET" },
                                  {
                                    prim: "IF_NONE",
                                    args: [
                                      [
                                        [
                                          {
                                            prim: "PUSH",
                                            args: [
                                              { prim: "string" },
                                              { string: "Get-item:55" },
                                            ],
                                          },
                                          { prim: "FAILWITH" },
                                        ],
                                      ],
                                      [],
                                    ],
                                  },
                                  { prim: "CDR" },
                                  { prim: "ADD" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "SOME" },
                                  { prim: "SWAP" },
                                  { prim: "UPDATE" },
                                  { prim: "SWAP" },
                                  { prim: "PAIR" },
                                  { prim: "PAIR" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DROP" },
                                  { prim: "SENDER" },
                                  { prim: "DIG", args: [{ int: "2" }] },
                                  { prim: "DUP" },
                                  { prim: "DUG", args: [{ int: "3" }] },
                                  { prim: "CDR" },
                                  { prim: "CAR" },
                                  { prim: "COMPARE" },
                                  { prim: "NEQ" },
                                  {
                                    prim: "IF",
                                    args: [
                                      [
                                        [
                                          { prim: "SENDER" },
                                          { prim: "DIG", args: [{ int: "3" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "4" }] },
                                          { prim: "CAR" },
                                          { prim: "CAR" },
                                          { prim: "COMPARE" },
                                          { prim: "NEQ" },
                                        ],
                                      ],
                                      [
                                        {
                                          prim: "PUSH",
                                          args: [
                                            { prim: "bool" },
                                            { prim: "False" },
                                          ],
                                        },
                                      ],
                                    ],
                                  },
                                  {
                                    prim: "IF",
                                    args: [
                                      [
                                        [
                                          { prim: "DIG", args: [{ int: "2" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "3" }] },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DUP" },
                                          { prim: "CAR" },
                                          { prim: "SWAP" },
                                          { prim: "CDR" },
                                          { prim: "DUP" },
                                          { prim: "DIG", args: [{ int: "5" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "6" }] },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "2" }] },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                [
                                                  {
                                                    prim: "PUSH",
                                                    args: [
                                                      { prim: "string" },
                                                      {
                                                        string:
                                                          "set_in_top-any",
                                                      },
                                                    ],
                                                  },
                                                  { prim: "FAILWITH" },
                                                ],
                                              ],
                                              [],
                                            ],
                                          },
                                          { prim: "DUP" },
                                          { prim: "CDR" },
                                          { prim: "SWAP" },
                                          { prim: "CAR" },
                                          { prim: "DIG", args: [{ int: "7" }] },
                                          { prim: "DUP" },
                                          { prim: "DUG", args: [{ int: "8" }] },
                                          { prim: "CAR" },
                                          { prim: "DIG", args: [{ int: "9" }] },
                                          { prim: "DUP" },
                                          {
                                            prim: "DUG",
                                            args: [{ int: "10" }],
                                          },
                                          { prim: "CAR" },
                                          { prim: "CDR" },
                                          { prim: "DIG", args: [{ int: "9" }] },
                                          { prim: "DUP" },
                                          {
                                            prim: "DUG",
                                            args: [{ int: "10" }],
                                          },
                                          { prim: "CDR" },
                                          { prim: "CAR" },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                [
                                                  {
                                                    prim: "PUSH",
                                                    args: [
                                                      { prim: "string" },
                                                      { string: "Get-item:51" },
                                                    ],
                                                  },
                                                  { prim: "FAILWITH" },
                                                ],
                                              ],
                                              [],
                                            ],
                                          },
                                          { prim: "CAR" },
                                          { prim: "SENDER" },
                                          { prim: "GET" },
                                          {
                                            prim: "IF_NONE",
                                            args: [
                                              [
                                                [
                                                  {
                                                    prim: "PUSH",
                                                    args: [
                                                      { prim: "string" },
                                                      { string: "Get-item:-1" },
                                                    ],
                                                  },
                                                  { prim: "FAILWITH" },
                                                ],
                                              ],
                                              [],
                                            ],
                                          },
                                          { prim: "SUB" },
                                          { prim: "SOME" },
                                          { prim: "SENDER" },
                                          { prim: "UPDATE" },
                                          { prim: "PAIR" },
                                          { prim: "SOME" },
                                          { prim: "SWAP" },
                                          { prim: "UPDATE" },
                                          { prim: "SWAP" },
                                          { prim: "PAIR" },
                                          { prim: "PAIR" },
                                          { prim: "DUG", args: [{ int: "3" }] },
                                          { prim: "DIG", args: [{ int: "2" }] },
                                          { prim: "DROP" },
                                        ],
                                      ],
                                      [[]],
                                    ],
                                  },
                                  { prim: "SWAP" },
                                  { prim: "DROP" },
                                ],
                              ],
                            ],
                          },
                        ],
                      ],
                    },
                  ],
                ],
              },
            ],
          ],
        },
        { prim: "PAIR" },
      ],
    ],
  },
];
