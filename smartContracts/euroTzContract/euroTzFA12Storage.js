import conf from "../../conf/conf";

export default {
  prim: "Pair",
  args: [
    {
      prim: "Pair",
      args: [{ string: conf.adminAddress }, []],
    },
    {
      prim: "Pair",
      args: [
        { prim: "False" },
        {
          prim: "Pair",
          args: [{ string: conf.whitelistContractAddress }, { int: "0" }],
        },
      ],
    },
  ],
};
