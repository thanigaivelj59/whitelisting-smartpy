import smartpy as sp

class euroTzFA12(sp.Contract):
    def __init__(self, admin, safelistAddress):
        self.init(
            paused = False, 
            balances = sp.big_map(), 
            administrator = admin, 
            totalSupply = 0, 
            safelistAddress = safelistAddress
            )
    
    '''
    Call Whitelist contract
    '''
    
    def assertTransfer(self, params):
        
        assertTrasnferRecord = sp.record(from_ = params.from_, to_ = params.to_)
        
        entryPointName = "assertTransfer"
        c = sp.contract(
                        t = sp.TRecord(from_ = sp.TAddress, to_ = sp.TAddress), 
                        address = self.data.safelistAddress, 
                        entry_point = entryPointName
                        ).open_some()
                        
        sp.transfer(assertTrasnferRecord, sp.mutez(0), c)
    
    def assertReceiver(self, address):
        
        entryPointName = "assertReceiver"
        
        c = sp.contract(
                        t = sp.TAddress, 
                        address = self.data.safelistAddress, 
                        entry_point = entryPointName
                        ).open_some()
                        
        sp.transfer(address, sp.mutez(0), c) 
    
    '''
    EntryPoints
    '''

    @sp.entry_point
    def transfer(self, params):
        sp.verify((sp.sender == self.data.administrator) |
            (~self.data.paused &
                ((params.f == sp.sender) |
                 (self.data.balances[params.f].approvals[sp.sender] >= params.amount))))
        self.assertTransfer(sp.record(from_ = params.f, to_ = params.t))         
        self.addAddressIfNecessary(params.t)
        sp.verify(self.data.balances[params.f].balance >= params.amount)
        self.data.balances[params.f].balance -= params.amount
        self.data.balances[params.t].balance += params.amount
        sp.if (params.f != sp.sender) & (self.data.administrator != sp.sender):
            self.data.balances[params.f].approvals[sp.sender] -= params.amount

    @sp.entry_point
    def approve(self, params):
        sp.verify((sp.sender == self.data.administrator) |
                  (~self.data.paused & (params.f == sp.sender)))
        alreadyApproved = self.data.balances[params.f].approvals.get(params.t, 0)
        sp.verify((alreadyApproved == 0) | (params.amount == 0))
        self.data.balances[params.f].approvals[params.t] = params.amount

    @sp.entry_point
    def setPause(self, params):
        sp.verify(sp.sender == self.data.administrator)
        self.data.paused = params

    @sp.entry_point
    def setAdministrator(self, params):
        sp.verify(sp.sender == self.data.administrator)
        self.data.administrator = params

    @sp.entry_point
    def mint(self, params):
        sp.verify(sp.sender == self.data.administrator)
        self.assertReceiver(params.address)
        self.addAddressIfNecessary(params.address)
        self.data.balances[params.address].balance += params.amount
        self.data.totalSupply += params.amount

    @sp.entry_point
    def burn(self, params):
        sp.verify(sp.sender == self.data.administrator)
        sp.verify(self.data.balances[params.address].balance >= params.amount)
        self.data.balances[params.address].balance -= params.amount
        self.data.totalSupply -= params.amount

    def addAddressIfNecessary(self, address):
        sp.if ~ self.data.balances.contains(address):
            self.data.balances[address] = sp.record(balance = 0, approvals = {})

    @sp.entry_point
    def getBalance(self, params):
        sp.transfer(sp.as_nat(self.data.balances[params.arg.owner].balance), sp.tez(0), sp.contract(sp.TNat, params.target).open_some())

    @sp.entry_point
    def getAllowance(self, params):
        sp.transfer(sp.as_nat(self.data.balances[params.arg.owner].approvals[params.arg.spender]), sp.tez(0), sp.contract(sp.TNat, params.target).open_some())

    @sp.entry_point
    def getTotalSupply(self, params):
        sp.transfer(sp.as_nat(self.data.totalSupply), sp.tez(0), sp.contract(sp.TNat, params.target).open_some())

    @sp.entry_point
    def getAdministrator(self, params):
        sp.transfer(self.data.administrator, sp.tez(0), sp.contract(sp.TAddress, params.target).open_some())

if "templates" not in __name__:
    @sp.add_test(name = "euroTzFA12")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("Minified euroTzFA12")

        admin = sp.address("tz1SVqTz7entj982jDSKcTQNgT7f2cg7C8dk")
        
        safelistAddress = sp.address("KT1LyMJVVZHQWGT4kJeNwBQzPBGrgsWCFxp1")


        c1 = euroTzFA12(admin, safelistAddress)

        scenario += c1
        